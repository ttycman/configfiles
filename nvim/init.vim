set nocompatible            " Disable compatibility to old-time vi
set showmatch               " Show matching brackets.
set ignorecase              " Do case insensitive matching

set mouse=v                 " middle-click paste with mouse
set hlsearch                " highlight search results
set tabstop=4               " number of columns occupied by a tab character
set softtabstop=4           " see multiple spaces as tabstops so <BS> does the right thing
set expandtab               " converts tabs to white space
set shiftwidth=4            " width for autoindents
set autoindent              " indent a new line the same amount as the line just typed
set number                  " add line numbers
set wildmode=longest,list   " get bash-like tab completions
set foldmethod=indent
set nofoldenable
set cc=120                  " set an 80 column border for good coding style
set smarttab
set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround

" podswietla szukane frazy
set hlsearch
set incsearch
set ignorecase
set smartcase

" wylacza kopie zapasowe (lepiej nie uzywaj)
set nobackup
set nowritebackup
set noswapfile

let mapleader = ","

" latwiejsze formatowanie paragrafow
vmap Q gq
vmap Q gqap

nnoremap <C-S-tab> :tabprevious<CR>
nnoremap <C-tab>   :tabnext<CR>
nnoremap <C-t>     :tabnew<CR>
inoremap <C-S-tab> <Esc>:tabprevious<CR>i
inoremap <C-tab>   <Esc>:tabnext<CR>i
inoremap <C-t>     <Esc>:tabnew<CR>


" wylaczenie podswietlenia CTRL+N
noremap <C-n> :nohl<CR>
vnoremap <C-n> :nohl<CR>
inoremap <C-n> :nohl<CR>

" szybki zapis CTRL+Z
noremap <C-Z> :update<CR>
vnoremap <C-Z> <C-C>:update<CR>
inoremap <C-Z> <C-O>:update<CR>

" ,e i ,E zamyka Vim
noremap <Leader>e :quit<CR>
noremap <Leader>E :qa!<CR>

" wygodne przelaczanie okien
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" przelaczanie zakladek
map <Leader>q <esc>:tabprevious<CR>
map <Leader>w <esc>:tabnext<CR>

" wygodne przemieszczanie blokow kodu w lewo i prawo
vnoremap < <gv
vnoremap > >gv


" toggle spelling
nnoremap <leader>s :set invspell<CR>

colorscheme wombat256

" path to your python
let g:python3_host_prog="/usr/bin/python3"
let g:python_host_prog="/usr/bin/python2"

" Vundle
" filetype off
" set the runtime path to include Vundle and initialize
set rtp+=~/.config/nvim/bundle/Vundle.vim
call vundle#begin('~/.config/nvim/bundle')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"Plugin 'vim-syntastic/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'majutsushi/tagbar'
Plugin 'farmergreg/vim-lastplace'
Plugin 'nvie/vim-flake8'
Plugin 'tpope/vim-surround'
"Plugin 'klen/python-mode'
Plugin 'ervandew/supertab'
Plugin 'scroolnoose/nerdcommenter'

Plugin 'Shougo/neosnippet'
Plugin 'Shougo/neosnippet-snippets'
Plugin 'vim-airline/vim-airline'  " make statusline awesome
Plugin 'vim-airline/vim-airline-themes'  " themes for statusline
Plugin 'jonathanfilip/vim-lucius'  " nice white colortheme
Plugin 'davidhalter/jedi-vim'   " jedi for python
Plugin 'tiagofumo/vim-nerdtree-syntax-highlight'  "to highlight files in nerdtree
Plugin 'Vimjas/vim-python-pep8-indent'  "better indenting for python
"Plugin 'kien/ctrlp.vim'  " fuzzy search files
"Plugin 'tweekmonster/impsort.vim'  " color and sort imports
Plugin 'wsdjeg/FlyGrep.vim'  " awesome grep on the fly
Plugin 'w0rp/ale'  " python linters
Plugin 'airblade/vim-gitgutter'  " show git changes to files in gutter
Plugin 'tpope/vim-commentary'  "comment-out by gc
Plugin 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plugin 'zchee/deoplete-jedi', { 'for': 'python' }
Plugin 'jiangmiao/auto-pairs'
"Plugin 'Valloric/YouCompleteMe'
"Plugin 'ncm2/ncm2'  " awesome autocomplete plugin
"Plugin 'roxma/nvim-yarp'  " dependency of ncm2
"Plugin 'ncm2/ncm2-bufword'  " buffer keyword completion
"Plugin 'ncm2/ncm2-path'  " filepath completion
"Plugin 'ncm2/ncm2-jedi'  " fast python completion (use ncm2 if you want type info or snippet support)
Plugin 'Chiel92/vim-autoformat'
Plugin 'roxma/vim-hug-neovim-rpc'


" All of your Plugins must be added before the following line
call vundle#end()
filetype plugin indent on  " allows auto-indenting depending on file type



" key maps
map <F4> :NERDTreeToggle<CR>
map <F12> :TagbarToggle<CR>

" cmds
command Cfg :e ~/.config/nvim/init.vim
command CFG :e! ~/.config/nvim/init.vim

" syntastic
"set statusline+=%#warningmsg#
"set statusline+=%{SyntasticStatuslineFlag()}
"set statusline+=%*
"
"let g:syntastic_always_populate_loc_list = 1
"let g:syntastic_auto_loc_list = 1
"let g:syntastic_check_on_open = 1
"let g:syntastic_check_on_wq = 0
"let g:syntastic_python_pylint_exe = 'python3 -m pylint3'

autocmd VimEnter * nested :call tagbar#autoopen(1)

let g:pymode_python = 'python3'

"let g:deoplete#enable_at_startup = 1

" ncm2 settings
"autocmd BufEnter * call ncm2#enable_for_buffer()
"set completeopt=menuone,noselect,noinsert
"set shortmess+=c
"inoremap <c-c> <ESC>
" make it fast
"let ncm2#popup_delay = 5
"let ncm2#complete_length = [[1, 1]]
" Use new fuzzy based matches
"let g:ncm2#matcher = 'substrfuzzy'

" Disable Jedi-vim autocompletion and enable call-signatures options
let g:jedi#auto_initialization = 1
let g:jedi#completions_enabled = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#smart_auto_mappings = 0
let g:jedi#popup_on_dot = 0
let g:jedi#completions_command = ""
let g:jedi#show_call_signatures = "1"

" highlight attribute access: hello.highlighted
autocmd BufEnter * syntax match Type /\v\.[a-zA-Z0-9_]+\ze(\[|\s|$|,|\]|\)|\.|:)/hs=s+1
" highlight function calls: hello.highlighted()
autocmd BufEnter * syntax match pythonFunction /\v[[:alnum:]_]+\ze(\s?\()/
hi def link pythonFunction Function
" Highlight self slightly darker
autocmd BufEnter * syn match Self "\(\W\|^\)\@<=self\(\.\)\@="
highlight self ctermfg=239
" Extra Impsort highlights:
hi pythonImportedObject ctermfg=127
hi pythonImportedFuncDef ctermfg=127
hi pythonImportedClassDef ctermfg=127

let g:airline_powerline_fonts = 1
let g:airline_section_y = ""
let g:airline#extensions#tabline#enabled = 1

" Airline settings
" do not show error/warning section
let g:airline_section_error = ""
let g:airline_section_warning = ""

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" FlyGrep settings
nnoremap <leader>s :FlyGrep<cr>

" easy breakpoint python
au FileType python map <silent> <leader>b ofrom pudb import set_trace; set_trace()<esc>
au FileType python map <silent> <leader>B Ofrom pudb import set_trace; set_trace()<esc>
au FileType python map <silent> <leader>j ofrom pdb import set_trace; set_trace()<esc>
au FileType python map <silent> <leader>J Ofrom pdb import set_trace; set_trace()<esc>

" ale options
let g:ale_python_flake8_options = '--ignore=E129,E501,E302,E265,E241,E305,E402,W503'
let g:ale_python_pylint_options = '-j 0 --max-line-length=120'
let g:ale_list_window_size = 4
let g:ale_sign_column_always = 0
let g:ale_open_list = 1
let g:ale_keep_list_window_open = '1'

" Options are in .pylintrc!
highlight VertSplit ctermbg=253

let g:ale_sign_error = '‼'
let g:ale_sign_warning = '∙'
let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_enter = '0'
let g:ale_lint_on_save = '1'
nmap <silent> <C-M> <Plug>(ale_previous_wrap)
nmap <silent> <C-m> <Plug>(ale_next_wrap)

" mapping to make movements operate on 1 screen line in wrap mode
function! ScreenMovement(movement)
   if &wrap
      return "g" . a:movement
   else
      return a:movement
   endif
endfunction

onoremap <silent> <expr> j ScreenMovement("j")
onoremap <silent> <expr> k ScreenMovement("k")
onoremap <silent> <expr> 0 ScreenMovement("0")
onoremap <silent> <expr> ^ ScreenMovement("^")
onoremap <silent> <expr> $ ScreenMovement("$")
nnoremap <silent> <expr> j ScreenMovement("j")
nnoremap <silent> <expr> k ScreenMovement("k")
nnoremap <silent> <expr> 0 ScreenMovement("0")
nnoremap <silent> <expr> ^ ScreenMovement("^")
nnoremap <silent> <expr> $ ScreenMovement("$")

" highlight python and self function
autocmd BufEnter * syntax match Type /\v\.[a-zA-Z0-9_]+\ze(\[|\s|$|,|\]|\)|\.|:)/hs=s+1
autocmd BufEnter * syntax match pythonFunction /\v[[:alnum:]_]+\ze(\s?\()/
hi def link pythonFunction Function
autocmd BufEnter * syn match Self "\(\W\|^\)\@<=self\(\.\)\@="
highlight self ctermfg=239

" deoplete + neosnippet + autopairs changes
let g:AutoPairsMapCR=0
let g:deoplete#auto_complete_start_length = 1
let g:deoplete#enable_at_startup = 1
let g:deoplete#enable_smart_case = 1
"imap <expr><TAB> pumvisible() ? "\<C-n>" : (neosnippet#expandable_or_jumpable() ? "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>")
"imap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<S-TAB>"
"imap <expr><CR> pumvisible() ? deoplete#smart_close_popup() : "\<CR>\<Plug>AutoPairsReturn"

augroup neovim
  autocmd!
  autocmd FileType vimfiler set nonumber | set norelativenumber
  autocmd Filetype * if &ft!='vimfiler' | set relativenumber | set number | endif
  autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif
  autocmd StdinReadPre * let s:std_in=1
  autocmd BufWritePre * %s/\s\+$//e
  autocmd TermClose * bw!
  "autocmd BufWinEnter,WinEnter term://* startinsert
  "autocmd BufLeave term://* stopinsert
  autocmd BufWritePost $MYVIMRC nested source $MYVIMRC
augroup END

3



"autocmd FileType javascript let g:SuperTabDefaultCompletionType = "<c-x><c-o>"
let g:UltiSnipsExpandTrigger="<C-j>"
inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"

" close the preview window when you're not using it
let g:SuperTabClosePreviewOnPopupClose = 1
" or just disable the preview entirely
set completeopt-=preview
