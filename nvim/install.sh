#!/bin/bash


mkdir bundle
cd bundle
git clone https://github.com/VundleVim/Vundle.vim.git
nvim +PluginInstall +qall
